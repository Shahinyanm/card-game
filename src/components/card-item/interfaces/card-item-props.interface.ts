export interface ICardItemProps {
    state: 'visible' | 'hidden';
    suit: 'diamonds' | 'hearts' | 'spades' | 'clubs';
    value: '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | 'J' | 'Q' | 'K' | 'A';
    isClosed?: boolean;
}